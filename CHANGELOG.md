# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0]

 - changes in the item model

## [v1.1.1] - [2021-09-28]

- solved bug on externalManaged field

## [v1.1.0] - [2021-04-29]

- jackson version moved to 2.8.11
- model for messages added

## [v1.0.10] - [2021-03-30]

- added relateNode to items (only fo internal service use)
- external managed folder added 
 

## [v1.0.9] - [2020-10-07]

StorageBackendFacory to support different backend added