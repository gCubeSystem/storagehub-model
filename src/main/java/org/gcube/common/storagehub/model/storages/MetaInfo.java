package org.gcube.common.storagehub.model.storages;

import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MetaInfo {
	
	long size;

	String storageId;
	
	String remotePath;
	
	PayloadBackend payloadBackend;
}
