package org.gcube.common.storagehub.model.items;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id" )
public abstract class RootItem {

	//for service internal use
	@JsonIgnore
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	transient Object relatedNode; 
	
	String id;

	String name;
	
	String path;
	
	String parentId;

	String parentPath;
	
	String primaryType;

}
