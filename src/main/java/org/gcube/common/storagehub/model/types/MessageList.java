package org.gcube.common.storagehub.model.types;

import java.util.List;
import org.gcube.common.storagehub.model.messages.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessageList {

	List<Message> messages;
	
}