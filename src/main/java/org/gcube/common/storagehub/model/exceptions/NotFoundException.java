package org.gcube.common.storagehub.model.exceptions;

public class NotFoundException extends StorageHubException {

	private static final long serialVersionUID = 1L;

	private String type;
	private String value;
	
	public NotFoundException(String type, String value) {
		super();
		this.type = type;
		this.value = value;
	}

	@Override
	public String getErrorMessage() {
		return String.format(" %s with value %s not found", this.type, this.value);
	}

	@Override
	public int getStatus() {
		return 404;
	}

}
