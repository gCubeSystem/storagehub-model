package org.gcube.common.storagehub.model.items.nodes;

import org.gcube.common.storagehub.model.annotations.Attribute;
import org.gcube.common.storagehub.model.annotations.AttributeRootNode;
import static org.gcube.common.storagehub.model.NodeConstants.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@AttributeRootNode(OWNERNODETYPE)
public class Owner {

	@Attribute(PORTALLOGIN)
	String userName;	
	
	@Attribute(UNIQUEID)
	String userId;
}
