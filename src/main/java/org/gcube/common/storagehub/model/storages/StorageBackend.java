package org.gcube.common.storagehub.model.storages;

import java.io.InputStream;
import java.util.Map;

import org.gcube.common.storagehub.model.exceptions.StorageIdNotFoundException;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;

public abstract class StorageBackend {
	
	private PayloadBackend payloadConfiguration;
	
	public StorageBackend(PayloadBackend payloadConfiguration) {
		this.payloadConfiguration = payloadConfiguration;
	}
	
	
	public PayloadBackend getPayloadConfiguration() {
		return payloadConfiguration;
	}
	
	protected void setPayloadConfiguration(PayloadBackend payloadConfiguration) {
		this.payloadConfiguration = payloadConfiguration;
	}


	public abstract MetaInfo onCopy(Content content, String newParentPath, String newName);
	
	public abstract MetaInfo onMove(Content content, String newParentPath);
	
	//public abstract void onDelete(Content content);
	
	public abstract void delete(String id);
	
	public abstract MetaInfo upload(InputStream stream, String relativePath, String name, String user);
	
	public abstract MetaInfo upload(InputStream stream, String relativePath, String name, Long size, String user);
	
	public abstract InputStream download(Content item) throws StorageIdNotFoundException;
	
	public abstract InputStream download(String id) throws StorageIdNotFoundException;
	
	public abstract Map<String, String> getFileMetadata(String id);
	
	public abstract MetaInfo upload(InputStream stream, String relativePath, String name, String storageId, Long size, String user);
	
	@Deprecated
	public abstract String getTotalSizeStored();
	@Deprecated
	public abstract String getTotalItemsCount();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((payloadConfiguration == null) ? 0 : payloadConfiguration.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StorageBackend other = (StorageBackend) obj;
		if (payloadConfiguration == null) {
			if (other.payloadConfiguration != null)
				return false;
		} else if (!payloadConfiguration.equals(other.payloadConfiguration))
			return false;
		return true;
	}
	
	

	
}
