package org.gcube.common.storagehub.model.plugins;

import java.util.Map;

import org.gcube.common.storagehub.model.exceptions.PluginInitializationException;
import org.gcube.common.storagehub.model.items.FolderItem;

public interface FolderManagerConnector {
	
	public default String getName() {
		return this.getClass().getCanonicalName(); 
	}
	
	FolderManager connect(FolderItem item, Map<String, Object> parameters) throws PluginInitializationException;
		
	
}
