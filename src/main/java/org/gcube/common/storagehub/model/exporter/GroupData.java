package org.gcube.common.storagehub.model.exporter;

import java.util.List;

import org.gcube.common.storagehub.model.acls.ACL;
import org.gcube.common.storagehub.model.acls.AccessType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class GroupData {

	@NonNull final String name;
	
	@NonNull final String folderOwner;
	
	@NonNull final List<String> members;
	
	@NonNull final AccessType accessType;
	
	@NonNull final List<ACL> acls;
	
	
	String fileRef = null;
}
