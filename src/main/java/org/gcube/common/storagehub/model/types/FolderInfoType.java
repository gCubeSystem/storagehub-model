package org.gcube.common.storagehub.model.types;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FolderInfoType {

	long size;
	long count;
	
}
