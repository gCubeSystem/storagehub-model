package org.gcube.common.storagehub.model.exporter;

import java.util.List;
import java.util.Map;

import org.gcube.common.storagehub.model.items.Item;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DumpData {

	List<UserData> users;
	List<GroupData> groups;
	
	Map<String, List<Item>> itemPerUser;
}
