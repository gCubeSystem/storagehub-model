package org.gcube.common.storagehub.model.items;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.gcube.common.storagehub.model.Metadata;
import static org.gcube.common.storagehub.model.NodeConstants.*;
import org.gcube.common.storagehub.model.annotations.Attribute;
import org.gcube.common.storagehub.model.annotations.NodeAttribute;
import org.gcube.common.storagehub.model.annotations.RootNode;

@NoArgsConstructor
@Getter
@Setter
@RootNode(SHAREDFOLDERTYPE)
public class SharedFolder extends FolderItem {

	@Attribute(SHAREDFOLDERPRIVILEGE)
	String privilege;
	
	@Attribute(VREFOLDERCHECK)
	boolean vreFolder;
		
	@Attribute(DISPLAYNAME)
	String displayName;  

	@NodeAttribute(USERS_NAME)
	Metadata users;
	
/*	@NodeAttribute("hl:members")
	Metadata members;
*/		
}
