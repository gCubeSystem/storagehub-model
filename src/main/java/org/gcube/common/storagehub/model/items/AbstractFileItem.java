package org.gcube.common.storagehub.model.items;

import org.gcube.common.storagehub.model.items.nodes.Content;

public abstract class AbstractFileItem extends Item{
			
	public abstract Content getContent();
			
}
