package org.gcube.common.storagehub.model.items.nodes;

import static org.gcube.common.storagehub.model.NodeConstants.PARAMETERS;
import static org.gcube.common.storagehub.model.NodeConstants.PAYLOADBACKENDTYPE;
import static org.gcube.common.storagehub.model.NodeConstants.STORAGENAME;

import java.util.Collections;
import java.util.Map;

import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.annotations.Attribute;
import org.gcube.common.storagehub.model.annotations.AttributeRootNode;
import org.gcube.common.storagehub.model.annotations.NodeAttribute;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@AttributeRootNode(PAYLOADBACKENDTYPE)
public class PayloadBackend {

	@Attribute(value= STORAGENAME)
	private String storageName;
	
	@NodeAttribute(value=PARAMETERS)
	private Metadata parameters = new Metadata();
	
	public String getStorageName() {
		return storageName;
	}
	
	@JsonIgnore
	public Map<String, Object> getParameters(){
		if (parameters ==null || parameters.getMap() == null )
			return Collections.emptyMap();
		else return parameters.getMap();
	}
	
}
