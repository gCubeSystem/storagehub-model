package org.gcube.common.storagehub.model;

public class NodeConstants {

	public static final String PARAMETERS_NAME ="hl:parameters";
	public static final String ACCOUNTING_NAME ="hl:accounting";
	public static final String METADATA_NAME ="hl:metadata";
	public static final String OWNER_NAME ="hl:owner";
	public static final String CONTENT_NAME ="jcr:content";
	public static final String USERS_NAME ="hl:users";
	public static final String EXTERNALREPOSITORY_NAME ="hl:externalRepository";
	public static final String PROPERTY_NAME ="hl:property";
	
	
	public static final String PUBLIC = "hl:isPublic";
	public static final String TITLE ="jcr:title";
	public static final String DESCRIPTION = "jcr:description";
	public static final String LASTMODIFIEDBY = "jcr:lastModifiedBy";
	public static final String LASTMODIFIEDTIME = "jcr:lastModified";
	public static final String CREATEDTIME = "jcr:created";
	public static final String PORTALLOGIN = "hl:portalLogin";
	public static final String LASTACTION = "hl:lastAction";
	public static final String HIDDEN = "hl:hidden";
	
	public static final String PAYLOADBACKEND ="hl:payloadBackend";
	
	public static final String SHAREDFOLDERPRIVILEGE = "hl:privilege";
	public static final String VREFOLDERCHECK = "hl:isVreFolder";
	public static final String DISPLAYNAME = "hl:displayName";
	
	public static final String UNIQUEID = "hl:uuid";
	public static final String PARAMETERS = "hl:parameters";
	
	public static final String CONTENTSIZE = "hl:size";
	public static final String CONTENTDATA ="jcr:data";
	public static final String CONTENTREMOTEPATH = "hl:remotePath";
	public static final String CONTENTMYMETYPE = "jcr:mimeType";
	public static final String CONTENTID = "hl:storageId";
	public static final String STORAGENAME = "hl:storageName";
	
	//TYPE 
	public static final String SHAREDFOLDERTYPE = "nthl:workspaceSharedItem";
	public static final String MESSAGETYPE = "nthl:itemSentRequest";
	public static final String OWNERNODETYPE = "nthl:user";
	public static final String GENERICFILETYPE = "nthl:externalFile";
	public static final String CONTENTFILETYPE = "nthl:file";
	public static final String PAYLOADBACKENDTYPE = "nthl:payloadBackend";
}
