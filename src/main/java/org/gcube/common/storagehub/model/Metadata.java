package org.gcube.common.storagehub.model;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.storagehub.model.annotations.MapAttribute;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Metadata {

	@MapAttribute(excludeStartWith="jcr:")
	Map<String, Object> map = new HashMap<String, Object>();
}
