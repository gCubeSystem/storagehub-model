package org.gcube.common.storagehub.model.items.nodes;


import static org.gcube.common.storagehub.model.NodeConstants.CONTENTDATA;
import static org.gcube.common.storagehub.model.NodeConstants.CONTENTFILETYPE;
import static org.gcube.common.storagehub.model.NodeConstants.CONTENTID;
import static org.gcube.common.storagehub.model.NodeConstants.CONTENTMYMETYPE;
import static org.gcube.common.storagehub.model.NodeConstants.CONTENTREMOTEPATH;
import static org.gcube.common.storagehub.model.NodeConstants.CONTENTSIZE;
import static org.gcube.common.storagehub.model.NodeConstants.PAYLOADBACKEND;

import org.gcube.common.storagehub.model.annotations.Attribute;
import org.gcube.common.storagehub.model.annotations.AttributeRootNode;
import org.gcube.common.storagehub.model.annotations.NodeAttribute;
import org.gcube.common.storagehub.model.storages.StorageNames;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@AttributeRootNode(CONTENTFILETYPE)
public class Content {

	@Attribute(CONTENTSIZE)
	Long size;
	
	@Attribute(CONTENTDATA)
	String data;
	
	@Attribute(CONTENTREMOTEPATH)
	String remotePath;
	
	@Attribute(CONTENTMYMETYPE)
	String mimeType;

	@Attribute(CONTENTID)
	String storageId;
		
	@NodeAttribute(value= PAYLOADBACKEND)
	PayloadBackend payloadBackend;
	
	public PayloadBackend getPayloadBackend(){
		if(payloadBackend == null) 
			return new PayloadBackend(StorageNames.MONGO_STORAGE, null);
		else return payloadBackend;
	}

}
