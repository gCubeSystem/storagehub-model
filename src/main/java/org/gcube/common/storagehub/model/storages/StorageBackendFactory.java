package org.gcube.common.storagehub.model.storages;

import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;

public interface StorageBackendFactory {
	
	String getName();
	
	default boolean isSystemStorage() {
		return false;
	}
	
	StorageBackend create(PayloadBackend payloadConfiguration) throws InvalidCallParameters;
	

}
