package org.gcube.common.storagehub.model.items;

import static  org.gcube.common.storagehub.model.NodeConstants.*;
import org.gcube.common.storagehub.model.annotations.NodeAttribute;
import org.gcube.common.storagehub.model.annotations.RootNode;
import org.gcube.common.storagehub.model.items.nodes.Content;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@RootNode(GENERICFILETYPE)
public class GenericFileItem extends AbstractFileItem{
	
	@NodeAttribute(value =CONTENT_NAME)
	Content content;

}
