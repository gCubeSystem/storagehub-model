package org.gcube.common.storagehub.model.service;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.storagehub.model.items.Item;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemList {

	@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
	private List<? extends Item> itemlist = new ArrayList<Item>();
}
