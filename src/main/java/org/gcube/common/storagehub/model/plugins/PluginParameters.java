package org.gcube.common.storagehub.model.plugins;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PluginParameters {

	private Map<String, Object> parameters = new HashMap<String, Object>();
	
	public Map<String, Object> getParameters() {
		return Collections.unmodifiableMap(parameters);
	}
	
	public void add(String name, String value) {
		this.parameters.put(name, value);
	}

	@Override
	public String toString() {
		return "PluginParameters [parameters=" + parameters + "]";
	}
	
	
}
