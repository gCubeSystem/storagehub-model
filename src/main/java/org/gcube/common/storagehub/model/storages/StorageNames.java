package org.gcube.common.storagehub.model.storages;

public class StorageNames {

	public static final String DEFAULT_S3_STORAGE = "default-gcube-s3";
	
    public static final String MONGO_STORAGE = "gcube-mongo";
	
	public static final String GCUBE_STORAGE = "gcube-s3";
}
