package org.gcube.common.storagehub.model.exceptions;

public class PluginNotFoundException extends StorageHubException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PluginNotFoundException() {
		super();
	}

	public PluginNotFoundException(String message) {
		super(message);
	}


	@Override
	public String getErrorMessage() {
		return "plugin not found";
	}

	@Override
	public int getStatus() {
		return 500;
	}

}
