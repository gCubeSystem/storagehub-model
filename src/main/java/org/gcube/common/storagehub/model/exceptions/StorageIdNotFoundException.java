package org.gcube.common.storagehub.model.exceptions;

public class StorageIdNotFoundException extends StorageHubException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7225259271771357699L;

	public StorageIdNotFoundException(String id, String storageName,  Throwable cause) {
		super(String.format("id %s not found in %s", id, storageName), cause);
	}

	public StorageIdNotFoundException(String id, String storageName) {
		super(String.format("id %s not found in %s", id, storageName));
	}
	
	@Override
	public String getErrorMessage() {
		return null;
	}

	@Override
	public int getStatus() {
		return 404;
	}

}
