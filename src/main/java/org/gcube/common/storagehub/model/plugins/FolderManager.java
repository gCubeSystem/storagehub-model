package org.gcube.common.storagehub.model.plugins;

import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.storages.StorageBackend;

public interface FolderManager {
	
	FolderItem getRootFolder();
	
	StorageBackend getStorageBackend();
	
	boolean manageVersion();
	
	void onCreatedFolder(FolderItem folder);
	
	void onDeletingFolder(FolderItem folder);

	void onMovedFolder(FolderItem movedFolder);
	
	void onCopiedFolder(FolderItem copiedFolder);
	
}

