package org.gcube.common.storagehub.model.items;

import static org.gcube.common.storagehub.model.NodeConstants.ACCOUNTING_NAME;
import static org.gcube.common.storagehub.model.NodeConstants.CREATEDTIME;
import static org.gcube.common.storagehub.model.NodeConstants.DESCRIPTION;
import static org.gcube.common.storagehub.model.NodeConstants.HIDDEN;
import static org.gcube.common.storagehub.model.NodeConstants.LASTACTION;
import static org.gcube.common.storagehub.model.NodeConstants.LASTMODIFIEDBY;
import static org.gcube.common.storagehub.model.NodeConstants.LASTMODIFIEDTIME;
import static org.gcube.common.storagehub.model.NodeConstants.METADATA_NAME;
import static org.gcube.common.storagehub.model.NodeConstants.OWNER_NAME;
import static org.gcube.common.storagehub.model.NodeConstants.PORTALLOGIN;
import static org.gcube.common.storagehub.model.NodeConstants.PUBLIC;
import static org.gcube.common.storagehub.model.NodeConstants.TITLE;

import java.util.Calendar;

import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.annotations.Attribute;
import org.gcube.common.storagehub.model.annotations.NodeAttribute;
import org.gcube.common.storagehub.model.items.nodes.Accounting;
import org.gcube.common.storagehub.model.items.nodes.Owner;
import org.gcube.common.storagehub.model.types.ItemAction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item extends RootItem{
	
	boolean trashed;
	
	boolean externalManaged = false;
	
	boolean shared;

	boolean locked;

	@Attribute(PUBLIC)
	boolean publicItem;
	
	@Attribute(TITLE)
	String title;

	@Attribute(DESCRIPTION)
	String description;

	@Attribute(LASTMODIFIEDBY)
	String lastModifiedBy;

	@Attribute(LASTMODIFIEDTIME)
	Calendar lastModificationTime;

	@Attribute(value = CREATEDTIME, isReadOnly=true)
	Calendar creationTime;

	@Attribute(PORTALLOGIN)
	String owner;

	@Attribute(LASTACTION)
	ItemAction lastAction;
	
	@Attribute(HIDDEN)
	boolean hidden;
	
	@NodeAttribute(value=OWNER_NAME, isReadOnly=true)
	Owner ownerNode;
	
	@NodeAttribute(value=ACCOUNTING_NAME, isReadOnly=true)
	Accounting accounting;

	@NodeAttribute(value=METADATA_NAME)
	Metadata metadata = new Metadata();
		
	public String getOwner() {
		if (owner!=null) return owner;
		else if (ownerNode!=null) return ownerNode.getUserName();
		return null;
	}
	
}
