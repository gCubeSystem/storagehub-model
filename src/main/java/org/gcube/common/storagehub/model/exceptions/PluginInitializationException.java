package org.gcube.common.storagehub.model.exceptions;

public class PluginInitializationException extends StorageHubException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PluginInitializationException() {
		super();
	}

	public PluginInitializationException(String message) {
		super(message);
	}


	@Override
	public String getErrorMessage() {
		return "invalid parameters for plugin";
	}

	@Override
	public int getStatus() {
		return 500;
	}

}
