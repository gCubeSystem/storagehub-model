package org.gcube.common.storagehub.model.messages;

import static org.gcube.common.storagehub.model.NodeConstants.*;

import java.util.Calendar;

import org.gcube.common.storagehub.model.annotations.Attribute;
import org.gcube.common.storagehub.model.annotations.NodeAttribute;
import org.gcube.common.storagehub.model.annotations.RootNode;
import org.gcube.common.storagehub.model.items.RootItem;
import org.gcube.common.storagehub.model.items.nodes.Owner;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RootNode(MESSAGETYPE)
public class Message extends RootItem{

	boolean withAttachments;
	
	@NodeAttribute(value=OWNER_NAME)
	Owner sender;
	
	@Attribute("hl:subject")
	String subject;
	
	@Attribute("hl:body")
	String body;
	
	@Attribute("hl:read")
	boolean read;
	
	@Attribute("hl:open")
	boolean opened;
	
	@Attribute("hl:addresses")
	String[] addresses;
	
	@Attribute(value = "jcr:created", isReadOnly=true)
	Calendar creationTime;
			
}
