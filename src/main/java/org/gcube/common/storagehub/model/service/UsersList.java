package org.gcube.common.storagehub.model.service;

import java.util.List;

import org.gcube.common.storagehub.model.types.SHUBUser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsersList {

	List<SHUBUser> users;
	
}

