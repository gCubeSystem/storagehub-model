package org.gcube.common.storagehub.model.items;

import static org.gcube.common.storagehub.model.NodeConstants.PAYLOADBACKEND;

import org.gcube.common.storagehub.model.annotations.NodeAttribute;
import org.gcube.common.storagehub.model.annotations.RootNode;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.StorageNames;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RootNode("nthl:workspaceItem")
public class FolderItem extends Item {

	@NodeAttribute(value= PAYLOADBACKEND)
	PayloadBackend backend = new PayloadBackend(StorageNames.DEFAULT_S3_STORAGE, null);
		
}
