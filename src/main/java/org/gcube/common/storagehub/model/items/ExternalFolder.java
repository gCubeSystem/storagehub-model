package org.gcube.common.storagehub.model.items;

import org.gcube.common.storagehub.model.annotations.RootNode;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RootNode("nthl:externalFolder")
public class ExternalFolder extends FolderItem {

	
}
