package org.gcube.common.storagehub.model.items;

import java.net.URL;

import org.gcube.common.storagehub.model.annotations.Attribute;
import org.gcube.common.storagehub.model.annotations.RootNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@RootNode({"nthl:externalLink","nthl:ExternalLink"})
public class ExternalLink extends Item{
		
	@Attribute("hl:value")
	URL value;	
	
}

